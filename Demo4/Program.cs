﻿using System;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exercice 1
            //Console.WriteLine("Veuillez entrer un premier nombre");
            //string entree1 = Console.ReadLine();
            //int nb1 = int.Parse(entree1);
            //Console.WriteLine("Veuillez entrer un second nombre");
            //string entree2 = Console.ReadLine();
            //int nb2 = int.Parse(entree2);
            //Console.WriteLine($"La somme est égale { nb1 + nb2 }");
            #endregion

            #region Exercice 2
            Console.WriteLine("Veuillez entrer un premier nombre");
            //string entree3 = Console.ReadLine();
            //int.TryParse(entree3, out int nb3);

            int nb3 = Convert.ToInt32(Console.ReadLine());
            //int nb3 = int.Parse(Console.ReadLine());

            Console.WriteLine("Veuillez entrer un second nombre");
            string entree4 = Console.ReadLine();
            int.TryParse(entree4, out int nb4);

            Console.WriteLine($"La somme est égale { nb3 + nb4 }");
            #endregion
        }
    }
}
