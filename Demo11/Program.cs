﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Demo11
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à 1 dimension
            // déclaration d'un tableau vide
            int[] tableau = new int[100];

            // déclaration d'un tableau non vide
            int[] tableau2 = { 1, 2, 42 };
            // int[] tableau3 = new int[] { 1, 2, 42 };

            // affection d'une cellule
            tableau2[2] = 43;

            // attention au dépassement des index de mon tableau
            //tableau2[3] = 43;

            //récupération d'une valeur dans une cellule
            Console.WriteLine(tableau2[2]);

            // récupérer la longueur d'un tableau
            Console.WriteLine(tableau2.Length);
            //Console.WriteLine(tableau2.Count());
            #endregion


            #region Matrice
            string[,] tableau3 = new string[3,5];
            int[,] tableau4 = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } }; //[3,2]
            // l'indice du tableau est une combinaison de plusieurs valeurs
            Console.WriteLine(tableau4[1,0]);

            // récupérer une des longueurs d'une matrice
            Console.WriteLine(tableau4.GetLength(0));
            #endregion

            #region Tableau de tableaux
            int[][] tabtab = new int[5][];
            tabtab[0] = new int[3];
            tabtab[1] = new int[4];
            #endregion

            // instanciation
            ArrayList l = new ArrayList();

            // ajout
            l.Add(42);
            l.Add("toto");
            // ajout de plusieurs elements
            l.AddRange(new ArrayList{ 1,2,3 });

            // suppresion d'un élément
            l.Remove(42);

            // suppression de le cellule à l'indice x
            l.RemoveAt(0);

            // crée une copie du tableau
            ArrayList l2 = (ArrayList)l.Clone();

            // copie l'adresse du tableau
            ArrayList l3 = l;

            // modifie l'émént à l'indice 2 avec une autre valeur
            l[2] = 45;
            Console.WriteLine(l[0]);


            Hashtable hTable = new Hashtable();

            hTable.Add("toto", 42);
            hTable.Add(true, "Khun");
            Console.WriteLine(hTable["toto"]);
            Console.WriteLine(hTable[true]);

            Queue q = new Queue();
            q.Enqueue(13);
            q.Enqueue("Toto");
            q.Enqueue(true);

            Console.WriteLine(q.Dequeue());


            List<string> list = new List<string>();
            list.Add("Khun");
            list.Add("Mike");
            list.Add("Thierry");
            list.Add("Piu");

            list.Remove("Thierry");
            list.RemoveAt(2);

            Console.WriteLine(list.Count);

            //Console.WriteLine(list.Count());

            /*
             * 1. Demander à l'utilisateur d'entrer des nombres entiers et les ajouter dans une liste, 
                si la valeur entrée n'est plus un nombre on arrete d'ajouter des nombres,
                et on calcule la moyenne
            * 2. Demander à l'utilisateur d'entrer des nombres entiers et les ajouter dans une liste, 
                si la valeur entrée n'est plus un nombre on arrete d'ajouter des nombres,
                et on trie la liste par ordre croissant
            */


        }
    }
}
