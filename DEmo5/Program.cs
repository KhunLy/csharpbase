﻿using System;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            //int age = 6;

            //if(age < 12)
            //{
            //    Console.WriteLine("Enfant");
            //}
            //else if(age < 18)
            //{
            //    Console.WriteLine("Ado");
            //}
            //else
            //{
            //    Console.WriteLine("Adulte");
            //}

            Console.WriteLine("Entrez un nombre"); // 3
            int.TryParse(Console.ReadLine(), out int nb);

            //// si une seule instruction {} sont facultatives
            //if (((nb / 2) + (nb / 2)) == nb)
            //// if(nb % 2 == 0)
            //    Console.WriteLine("Pair");
            //else
            //    Console.WriteLine("Impair");

            // opérateur ternaire
            Console.WriteLine(nb % 2 == 0 ? "pair" : "impair");
        }



    }
}
