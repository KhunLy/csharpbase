﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ecrire dans la console sans retour à la ligne
            Console.Write("Hello World!!!");
            // Ecrire dans la console avec retour à la ligne
            Console.WriteLine("Hello World!!!");
            // shortcut => cw + tab + tab
            Console.WriteLine("42");

            int a = 42;

            int b = 43;

            // string format
            Console.WriteLine("{0} + {1} = {2}", a, b, a+b);

            // string interpolation
            Console.WriteLine($"{a} + {b} = {a+b}");

            // caractère d'échappement \
            Console.WriteLine("\"");

            Console.WriteLine("\\");

            // string sans échappement possible
            Console.WriteLine(@"c:\desktop");

            // vider la console
            //Console.Clear();
            Console.ReadKey();
        }
    }
}
