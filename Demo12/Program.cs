﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo12
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> l = new List<int>();

            Console.WriteLine("Entrez un nombre");
            string input = Console.ReadLine();

            while (int.TryParse(input, out int nb))
            {
                l.Add(nb);
                Console.WriteLine("Entrez un nombre");
                input = Console.ReadLine();
            }

            //Console.WriteLine(l.Average());

            //float sum = 0;

            //foreach(int nb in l) {
            //    sum += nb;
            //}

            //Console.WriteLine($"La moyenne est { sum / l.Count }");
            bool sorted = false;
            int compt = 1;
            while (!sorted)
            {
                sorted = true;
                for (int i = 0; i < l.Count - compt; i++)
                {
                    if (l[i] > l[i + 1])
                    {
                        // inversion des valeurs
                        int temp = l[i];
                        l[i] = l[i + 1];
                        l[i + 1] = temp;
                        sorted = false;
                    }
                }
                compt++;
            }
            //[1,2,3,4,5]
        }
    }
}
