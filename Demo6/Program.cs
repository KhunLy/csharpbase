﻿using System;
using System.Collections.Generic;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 42;
            // ajouter une unité
            a++; // ++a;

            // ajoute 3 à la variable a
            a += 3; // a = a + 3

            // retire une unité
            a--; // --a;

            // multiplie la variable par 6 
            a *= 6; // a = a * 6

            int b = 8; // 100 => 010 
            b = b >> 1;
            Console.WriteLine(b);

            // coalesce
            string nom = null;

            // null safe operator 
            Console.WriteLine(nom?.ToUpper() ?? "Aucun nom renseigné");

            // recupére le type d'un type
            Type t = typeof(int);

            object o = new object();
            //if(o is int)
            //{
            //    int i = (int)o;
            //}
            if(o is int i)
            {

            }

        }
    }
}
