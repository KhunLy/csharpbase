﻿using System;

namespace Demo7
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Calcul de la division entière, du modulo, de la division
            //Console.WriteLine("Entrer un premier nombre");
            //int.TryParse(Console.ReadLine(), out int nb1);

            //Console.WriteLine("Entrer un second nombre");
            //int.TryParse(Console.ReadLine(), out int nb2);

            //if(nb2 == 0)
            //{
            //    Console.WriteLine("Calculs impossibles");
            //}
            //else
            //{
            //    Console.WriteLine($"division entière: { nb1 / nb2 }");
            //    Console.WriteLine($"modulo: { nb1 % nb2 }");
            //    Console.WriteLine($"division: { (float)nb1 / nb2 }");
            //}
            #endregion

            Console.WriteLine("Entrez un BBAN (123-1234567-42)");

            string bban = Console.ReadLine();

            if(bban.Length != 12)
            {
                Console.WriteLine("KO");
            }

            //long.TryParse(bban, out long value);
            //long gauche = value / 100;
            //long droite = value % 100;

            long.TryParse(bban.Substring(0, 10), out long gauche);
            long.TryParse(bban.Substring(10, 2), out long droite);

            if(gauche % 97 == droite || (gauche % 97 == 0 && droite == 97))
            {
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine("KO");
            }
        }
    }
}
