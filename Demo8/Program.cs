﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            // boucle for
            // on prefera utiliser cette boucle qd on connait le nombre d'iterations
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            // on prefera utiliser la boucle while qd on ne connait le nombre d'iterations
            int j = 0;
            while(j < 10)
            {
                Console.WriteLine(j);
                j++;
            }

            //string[] noms = new string[] { "Khun", "Mike", "Thierry" };

            List<string> noms = new List<string> { "Khun", "Mike", "Thierry" };

            // boucle foreach
            // remarque on ne peut itérer que sur des objets "IEnumerable"
            foreach (string item in noms)
            {
                Console.WriteLine(item);
            }
        }
    }
}
