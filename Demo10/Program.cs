﻿using System;
using System.Collections.Generic;

namespace Demo10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tableau = new int[100];

            // index des tableau commence à 0 
            tableau[42] = 99;
            Console.WriteLine(tableau[42]);

            // longueur d'un tableau
            Console.WriteLine(tableau.Length);

            // tableau à 2 dimensions
            int[,] matrice = new int[10, 12];

            // longueur d'une matrice
            Console.WriteLine(matrice.GetLength(0));

            // index tableau à x dimensions
            matrice[4, 5] = 56;



            //Dictionary<string, string> dico = new Dictionary<string, string> {
            //    { "Hello", "Bonjour" },
            //    { "Thanks", "Merci" }
            //};

            //foreach (string value in dico.Keys)
            //{
            //    Console.WriteLine(value);
            //}


            List<string> profs = new List<string>();

            profs.Add("Khun");
            profs.Add("Thierry");

            profs.Remove("Khun");

            Dictionary<string, string> dico = new Dictionary<string, string>();
            dico.Add("Jan", "Janvier");
            dico["Jan"] = "January";

        }
    }
}
