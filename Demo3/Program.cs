﻿using System;

namespace Demo3
{
    class Program
    {
        // déclaration d'1 variable membre
        // conventio de nommage des variables => lowerCamelCase
        static int maVariable;

        // déclaration des constantes
        const int nbConstant = 42;
        // les variables readonly peuvent être initialisée dans le constructeur
        readonly int nbEnLectureSeule;

        public Program()
        {
            nbEnLectureSeule = 42;
        }

        static void Main(string[] args)
        {
            // déclaration d'une variable locale
            int monAutreVariable;

            // les variables membres sont initialisés avec la valeur par défaut du type
            Console.WriteLine(maVariable);


            Console.WriteLine(default(string));

            string chaine = null;

            int nb = int.MaxValue;

            // dépassement de la valeur max des entiers => négatif
            Console.WriteLine(nb + 1);

            // pas possible sur les types (valeurs)
            //int nb = null;

            // il est toutefois possible de changer le type int en utilisant un autre autre int? ou Nullable<int>

            //Nullable<int> nb = null;

            // ce n'est pas le cas pour les variables locales
            //Console.WriteLine(monAutreVariable);

            // initialisation de la variable
            monAutreVariable = 42;

            // valeur vs ref
            int[] a = new int[1]; // [42]
            a[0] = 42;

            int[] b = a;

            a[0] = 45;
            Console.WriteLine(b[0]);

            //Change(ref a);
            //Console.WriteLine(a[0]);

            // mot clé var 
            var variable = 1F;

            // séparateur de millliers
            long milliard = 1_000_000_000;

            // conversion long => string
            string milliardEnString = milliard.ToString();

            // conversion string => int
            string value = "42";

            int entier = int.Parse(value);
            // il est impossible dans certain cas de transformer une chaine en un autre type = > erreur
            //bool boolean = bool.Parse(value);
            //DateTime date = DateTime.Parse(value);

           
            int.TryParse(value, out int nombre);
            Console.WriteLine(nombre + 5);

            // conversion implicite

            int nb3 = 42;
            long nb4 = nb3;


            // casting explicite
            int nb5 = (int)(100F / 208 * 45);
            float f1 = 21.45F;
            int nb6 = (int)f1;

            long? nb7 = 45;
            int? nb8 = (int?)nb7;
            int? nb9 = nb7 as int?;

            Console.WriteLine((int)f1);

        }

        //private static void Change(ref int[] a)
        //{
        //    a = new int[] { 45 };
        //}

        //static void AutreMethode()
        //{
        //    maVariable = 42;
        //    //attention à la portée des variables;
        //    //monAutreVariable = 45;
        //}
    }
}
